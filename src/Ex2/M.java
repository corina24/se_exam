package Ex2;


class K {
}



////clasa M are un picior B ( compozitie)
class M extends K { //// M - ul este si clasa principala de unde pornesc executia
    B componenta = new B();

    public static void main(String[] args) {
        L asociere = new L();
    }

}



class B {
    public void metB() {
    }
}



class A {  /// clasa M e in relatie de agregare cu clasa A
    M obiectAgregare;

    A(M obiect) {
        this.obiectAgregare = obiect;
    }

    public void metA() {
    }
}



interface X {
}



class L implements X {
    private int a;

    public void i(X x) {
        System.out.println("Ma leg de interfata");
    }
}
