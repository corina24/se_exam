package Ex1;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class Test {
    ///sa numar 6 iteratii fara sa imi fie incrementat de mai multe ori sau sa fac problema ca la producer - consumer
    public static AtomicInteger k = new AtomicInteger();

    public static void main(String[] args) {   ///i need the main method to launch my app
        MyClass threadA = new MyClass();
        MyClass threadB = new MyClass();
        MyClass threadC = new MyClass();
        threadA.setName("NewThread 1");
        threadB.setName("NewThread 2");
        threadC.setName("NewThread 3");

        threadA.run();
        threadB.run();
        threadC.run();

    }
}


class MyClass extends Thread {

    Date today;
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    ///ar trebui sa fac un fel de bariera in care, desi am 3 thread- uri, chiar daca nu tin cont de sincronizare, sa astept sa imi vina 6 thread- uri si apoi sa fac un join si sa inchei executia
    public void run() {
        if (Test.k.getAndIncrement() < 6) { ///cumva nu iese ca nu reusesc sa pastrez thread - urile alive - in stare de ready
            try {
                today = new Date();
                System.out.println(this.getName() + " - la data: " + dateFormat.format(today.getTime()));
                sleep(1000); ///afisez la 1 secunda
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
